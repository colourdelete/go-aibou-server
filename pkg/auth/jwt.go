package auth

import (
	"log"

	"gitlab.com/colourdelete/go-aibou-server/pkg/types"
	"golang.org/x/xerrors"

	jwt "github.com/appleboy/gin-jwt/v2"
	"github.com/gin-gonic/gin"
	"gitlab.com/colourdelete/go-aibou-server/pkg/data"
)

var JWTAuthBackend AuthBackend = TestNoHashAuth

type JWTUser struct {
	ID string
}

type JWTAuthBind struct {
	ID   string `json:"id" binding:"required"`
	Pass string `json:"pass" binding:"required"`
}

const IDKey = "aibou_id"

func payload(data interface{}) jwt.MapClaims {
	if v, ok := data.(*JWTUser); ok {
		return jwt.MapClaims{
			IDKey: v.ID,
		}
	}
	return jwt.MapClaims{}
}

func identityHandler(c *gin.Context) interface{} {
	claims := jwt.ExtractClaims(c)
	return &JWTUser{
		ID: claims[IDKey].(string),
	}
}

func authenticator(c *gin.Context) (interface{}, error) {
	var authBind JWTAuthBind
	if err := c.ShouldBind(&authBind); err != nil {
		// invalid auth request
		return nil, err
		//return nil, jwt.ErrMissingLoginValues
	}

	if ok := JWTAuthBackend.CheckIDPBKDF2(authBind.ID, authBind.Pass); ok {
		// auth successful
		return &JWTUser{ID: authBind.ID}, nil
	}
	// auth unsuccessful
	return nil, jwt.ErrFailedAuthentication
}

func authorizator(data interface{}, _ *gin.Context) bool {
	if jwtUser, ok := data.(*JWTUser); ok && JWTAuthBackend.CheckID(jwtUser.ID) {
		// user id ok
		return true
	}
	return false
}

func unauthorized(c *gin.Context, code int, message string) {
	c.JSON(code, types.Resp{
		Msg:  message,
		Data: nil,
	}.Map())
}

func SetupJWT(app *gin.Engine, middleware bool) gin.HandlerFunc {
	jwtMiddleware, err := jwt.New(&jwt.GinJWTMiddleware{
		Realm:           data.Realm,
		Key:             data.SecretKey,
		Timeout:         data.Timeout,
		MaxRefresh:      data.MaxRefresh,
		IdentityKey:     IDKey,
		PayloadFunc:     payload,
		IdentityHandler: identityHandler,
		Authenticator:   authenticator,
		Authorizator:    authorizator,
		Unauthorized:    unauthorized,
		TokenLookup:     "header: Authorization, query: token, cookie: jwt",
	})

	if err != nil {
		log.Fatal(xerrors.Errorf("JWT new: %w", err))
	}

	if errInit := jwtMiddleware.MiddlewareInit(); errInit != nil {
		log.Fatal(xerrors.Errorf("JWT init: %w", errInit))
	}

	middlewareFunc := jwtMiddleware.MiddlewareFunc()
	app.GET("/auth", middlewareFunc, func(c *gin.Context) {
		claims := jwt.ExtractClaims(c)
		_, exists := c.Get(IDKey)
		if !exists {
			c.JSON(200, types.Resp{
				Msg:  "not authenticated",
				Data: nil,
			})
			return
		}
		c.JSON(200, types.Resp{
			Msg: "authenticated",
			Data: gin.H{
				"id": claims[IDKey],
			},
		})
	})
	app.POST("/auth", jwtMiddleware.LoginHandler)
	app.POST("/auth/refresh", middlewareFunc, jwtMiddleware.RefreshHandler)
	if middleware {
		app.Use(middlewareFunc)
	}
	//app.NoRoute(middlewareFunc, func(c *gin.Context) {
	//	claims := jwt.ExtractClaims(c)
	//	log.Printf("NoRoute claims: %#v\n", claims)
	//	c.JSON(404, types.Resp{
	//		Msg: "not found",
	//	})
	//})
	return middlewareFunc
}

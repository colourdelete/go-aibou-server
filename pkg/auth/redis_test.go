package auth

//import (
//	"testing"
//)
//
//func RedisTest(db map[string]string) *Redis {
//	return &Redis{
//		getFunc: func(key string) (string, error) {
//			if val, ok := db[key]; ok {
//				return val, nil
//			} else {
//				return "", nil
//			}
//		},
//	}
//}
//
//func TestRedis_CheckID(t *testing.T) {
//	const (
//		username    = "username"
//		badUsername = "bad"
//		password    = "password"
//	)
//	r := RedisTest(map[string]string{
//		username: string(PBKDF2Config{
//			Pass:   []byte(password),
//			Salt:   Salt(PBKDF2DefaultSaltLen),
//			Iter:   PBKDF2DefaultIter,
//			KeyLen: PBKDF2DefaultKeyLen,
//		}.ToJSON()),
//	})
//	if !r.CheckID(username) {
//		t.Fatal(".CheckID must be true -> user must be present")
//	}
//	if !r.CheckIDPBKDF2(username, password) {
//		t.Fatal(".CheckIDPBKDF2 must be true -> user & pass pair must be valid")
//	}
//	if r.CheckID(badUsername) {
//		t.Fatal(".CheckID must be false -> user must not be present")
//	}
//	if r.CheckIDPBKDF2(badUsername, password) {
//		t.Fatal(".CheckIDPBKDF2 must be false -> user & pass pair must not be valid")
//	}
//}

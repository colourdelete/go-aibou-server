package auth

var TestNoHashAuth = TestNoSecurity{}

type TestNoSecurity struct{}

func (t TestNoSecurity) Close() error {
	return nil
}

var _ AuthBackend = TestNoSecurity{}

func (t TestNoSecurity) CheckID(id string) bool {
	return true
}

func (t TestNoSecurity) CheckIDPBKDF2(id string, hash string) bool {
	return true
}

package db

import (
	"fmt"

	"gitlab.com/colourdelete/go-aibou-server/pkg/types"

	"github.com/gin-gonic/gin"
	"github.com/piaohao/godis"
	"github.com/sirupsen/logrus"
	"gitlab.com/colourdelete/go-aibou-server/pkg/uid"
)

type Redis struct {
	opt     godis.Option
	client  *godis.Redis
	getFunc func(string) (string, error)
}

func (r *Redis) Log(entry logrus.Entry) error {
	logrus.Warn("not implemented")
	logrus.Warn(entry)
	return nil
}

var _ Backend = &Redis{}

func NewRedis(opt godis.Option) *Redis {
	client := godis.NewRedis(&opt)
	if gin.Mode() == gin.DebugMode {
		logrus.Warn("not ready for production")
		_, err := client.Set( // SHA3-512 of "username"
			"user-94e89bd380edb3493e982813bf943696c49b68021410d7ac0ac0b5ebc8fb3e033a063e94760afe76ed933c17e2e00287a781c7887115ef927634bfd412299b2c",
			string(types.User{
				Username: "94e89bd380edb3493e982813bf943696c49b68021410d7ac0ac0b5ebc8fb3e033a063e94760afe76ed933c17e2e00287a781c7887115ef927634bfd412299b2c",
				Allow:    nil,
				Block:    nil,
			}.ToJSON()),
		)
		if err != nil {
			logrus.Fatal(err)
		}
	}
	return &Redis{
		opt:    opt,
		client: client,
		getFunc: func(s string) (string, error) {
			return client.Get(fmt.Sprintf("user-%s", s))
		},
	}
}

func (r *Redis) User(id uid.ID) (types.User, error) {
	val, err := r.getFunc(string(id))
	if err != nil {
		return types.User{}, err
	}
	return types.FromJSON([]byte(val)), nil
}

func (r *Redis) Close() error {
	return r.client.Close()
}

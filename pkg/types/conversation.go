package types

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"strings"
	"sync"
	"time"

	"github.com/sirupsen/logrus"

	gpt_3 "gitlab.com/colourdelete/go-aibou-server/pkg/gpt-3"

	goaway "github.com/TwinProduction/go-away"

	//"github.com/PullRequestInc/go-gpt3"
	"gitlab.com/colourdelete/go-aibou-server/pkg/uid"
)

const tokenLimit uint = 4000

var lockGPT3 GPT3Lock

type GPT3Lock struct {
	sync.Mutex
}

func (g *GPT3Lock) LockAndWait() {
	g.Lock()
	g.Wait()
}

func (g *GPT3Lock) Wait() {
	now := time.Now()
	if now.After(prevGPT3.Add(cooldown)) {
		time.Sleep(prevGPT3.Add(cooldown).Sub(now))
	}
}

var prevGPT3 time.Time

const cooldown = 10 * time.Second

func init() {
	prevGPT3 = time.Now()
}

var Conversations = map[uid.ID]*Conversation{}

type Conversation struct {
	Meta     ConvMeta          `json:"meta"`
	Msgs     *Msgs             `json:"messages"`
	Scenario Scenario          `json:"-"`
	Actions  map[uid.ID]Action `json:"actions"`
	Chars    Characters        `json:"message_types"`
	State    string            `json:"state"`
	//client   gpt3.Client
	client2 *gpt_3.Client
}

func (c *Conversation) String() string {
	return fmt.Sprintf("%s, %s, %s", c.Meta, c.Msgs, c.Scenario)
}

func (c *Conversation) RenderForGPT3() string {
	return c.Msgs.RenderForGPT3(c.Chars)
}

func (c *Conversation) ensureClient() {
	//if c.client == nil {
	//	c.client = c.Scenario.GPT3.NewClient()
	//}
	// client available?
	if c.client2 == nil {
		// no, make new client
		c.client2 = gpt_3.NewClient(
			&http.Client{Timeout: 5 * time.Second},
			c.Scenario.GPT3.APIKey,
			nil, // not using multiple orgs anyway
		)
	}
}

func (c *Conversation) checkTokenLimit(tokenLimit uint) error {
	if c.Msgs.ApproxTokens(c.Chars) > tokenLimit {
		return fmt.Errorf("token limit exceeded (%v)", tokenLimit)
	}
	return nil
}

func (c *Conversation) RateAll() error {
	for i, msg := range *c.Msgs {
		if msg.Rating != nil {
			continue
		}
		// filter text
		level, err := c.ContentFilter(msg.Content)
		if err != nil {
			return err
		}
		switch level {
		case 1, 2: // i just love switches
			// "sensitive" or "unsafe", so don't show
			return errors.New("unsafe or sensitive")
		}
		(*c.Msgs)[i].Rating = &level
	}
	return nil
}

func (c *Conversation) Refresh() error {
	lockGPT3.LockAndWait()
	defer lockGPT3.Unlock()

	// check token limit
	err := c.checkTokenLimit(tokenLimit)
	if err != nil {
		return err
	}

	// ensure client is available
	c.ensureClient()

	prompt := c.RenderForGPT3()
	stop := "\n"
	resp, err := c.client2.CreateCompletion(context.Background(), gpt_3.CreateCompletionReq{
		Engine:           c.Scenario.GPT3.Engine,
		Prompt:           &prompt,
		MaxTokens:        &c.Scenario.GPT3.MaxTokens,
		Temperature:      &c.Scenario.GPT3.Temperature,
		TopP:             &c.Scenario.GPT3.TopP,
		FrequencyPenalty: &c.Scenario.GPT3.FrequencyPenalty,
		PresencePenalty:  &c.Scenario.GPT3.PresencePenalty,
		Stop:             &stop, // TODO: configurable stop
		User:             "test",
	})
	if err != nil {
		return err
	}

	var gpt3ID uid.ID
	found := false
	// get ID of first GPT-3 char
	for id, char := range c.Chars {
		if char.Type == CharacterTypeGpt3 {
			gpt3ID = id
		}
		found = true
	}
	// GPT-3 char not found?
	if !found {
		return errors.New("corrupt conversation: no GPT3 character found")
	}
	// no choices?
	if len(resp.Choices) != 1 {
		return errors.New("cannot use GPT3 response: no choices")
	}
	logrus.Print(c.Msgs.RenderForGPT3(c.Chars))
	// append message
	c.Msgs.Append(Msg{
		ID:      uid.New(),
		From:    gpt3ID,
		Content: resp.Choices[0].Text,
	})
	logrus.Print("appended", c.Msgs.RenderForGPT3(c.Chars))

	// rate messages (including appended one)
	err = c.RateAll()
	logrus.Print("Rated", c.Msgs.RenderForGPT3(c.Chars))
	return err

	// old code
	//resp, err := c.client.CompletionWithEngine(context.Background(), c.Scenario.GPT3.Engine, gpt3.CompletionRequest{
	//	Prompt:           []string{c.RenderForGPT3()},
	//	MaxTokens:        &c.Scenario.GPT3.MaxTokens,
	//	Temperature:      &c.Scenario.GPT3.Temperature,
	//	TopP:             &c.Scenario.GPT3.TopP,
	//	N:                nil,
	//	LogProbs:         nil,
	//	PresencePenalty:  c.Scenario.GPT3.PresencePenalty,
	//	FrequencyPenalty: c.Scenario.GPT3.FrequencyPenalty,
	//})
	//if err != nil {
	//	return err
	//}
	//
	//var gpt3ID uid.ID = ""
	//for id, char := range c.Chars {
	//	if char.Type == CharacterTypeGpt3 {
	//		gpt3ID = id
	//	}
	//}
	//
	//if gpt3ID == "" {
	//	return errors.New("corrupt conversation: no GPT3 character found")
	//}
	//
	//if len(resp.Choices) != 1 {
	//	return errors.New("cannot use GPT3 response: no choices")
	//}
	//
	//text := resp.Choices[0].Text
	//
	//level, err := c.ContentFilter(text)
	//if err != nil {
	//	return xerrors.Errorf("Content Filter: %w", err)
	//}
	//
	//switch level {
	//case 0:
	//	//text = "safe\n" + text
	//case 1:
	//	//text = "sensitive"
	//	text = "不適切な内容が含まれていたので、表示することができません。リロードしてください。"
	//case 2:
	//	//text = "unsafe"
	//	text = "不適切な内容が含まれていたので、表示することができません。リロードしてください。"
	//default:
	//	//text = "unsafe"
	//	text = "不適切な内容が含まれていたので、表示することができません。リロードしてください。"
	//}
	//
	//c.Msgs.Append(Msg{
	//	ID:      uid.New(),
	//	From:    gpt3ID,
	//	Content: text,
	//})
	//
	//return nil
}

//var contentFilterURL *url.URL

//func init() {
//	var err error
//	contentFilterURL, err = url.Parse("https://api.openai.com/v1/engines/content-filter-alpha-c4/completions")
//	if err != nil {
//		panic(err)
//	}
//}

//type contentFilterResp struct {
//	ID      string `json:"id"`
//	Object  string `json:"object"`
//	Created int    `json:"created"`
//	Model   string `json:"model"`
//	Choices []struct {
//		Text         string                 `json:"text"`
//		Index        int                    `json:"index"`
//		Logprobs     map[string]interface{} `json:"logprobs"`
//		FinishReason string                 `json:"finish_reason"`
//	} `json:"choices"`
//}

func (c *Conversation) ContentFilter(text string) (level int, err error) {
	err = c.checkTokenLimit(tokenLimit)
	if err != nil {
		return
	}

	// check profane words using goaway
	if goaway.IsProfane(text) {
		level = 2
		return
	}

	// ensure client is available
	c.ensureClient()
	level, err = c.client2.ContentFilter(context.Background(), text)
	if err != nil {
		return
	}
	return

	// Old code
	// ========
	//// Use direct HTTP request since library doesn't work
	//req, err := http.NewRequest(
	//	http.MethodPost,
	//	contentFilterURL.String(),
	//	bytes.NewBufferString(fmt.Sprintf(
	//		`{"prompt": "<|endoftext|>%s\n--\nLabel:", "temperature": 0, "max_tokens": 1, "top_p":0, "logprobs": 10}`,
	//		text,
	//	)),
	//)
	//if err != nil {
	//	return -1, err // -1 is not 0, 1, 2 so should not work even in error is ignored
	//}
	//
	//// Set API Key header (Bearer [api key])
	//req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", c.Scenario.GPT3.APIKey))
	//req.Header.Set("Content-Type", "application/json")
	//
	//resp, err := (&http.Client{Timeout: 2 * time.Second}).Do(req)
	//if err != nil {
	//	return -1, err
	//}
	//
	//var data contentFilterResp
	//err = json.NewDecoder(resp.Body).Decode(&data)
	//if err != nil {
	//	return -1, err
	//}
	//
	//// TODO: handle index out of bounds
	//label := data.Choices[0].Text
	//
	//switch label {
	//case "0":
	//	return 0, nil
	//case "1":
	//	return 1, nil
	//case "2": // even if unsure, dont care for now // TODO: handle logprobs
	//	return 2, nil
	//default:
	//	return 2, nil
	//}
	//
	////// not consts since you can't take pointers of consts
	////maxTokens := 1
	////temp := float32(0)
	////topP := float32(0)
	////logProbs := 10
	////req := gpt3.CompletionRequest{
	////	Prompt:      []string{fmt.Sprintf("<|endoftext|>%s\n--\nLabel:", text)},
	////	MaxTokens:   &maxTokens,
	////	Temperature: &temp,
	////	TopP:        &topP,
	////	LogProbs:    &logProbs,
	////}
	////resp, err := c.client.CompletionWithEngine(context.Background(), "content-filter-alpha-c4", req)
	////
	////logrus.WithFields(logrus.Fields{
	////	"req": req,
	////	"err": err,
	////}).Error("OpenAI content filter error")
	////
	////if err != nil {
	////	return 0, err
	////}
	////
	////if len(resp.Choices) != 1 {
	////	return 0, errors.New("cannot use GPT3 response: no choices")
	////}
	////
	////choice := resp.Choices[0]
	////output := choice.Text
	////
	////switch output {
	////case "0":
	////	return 0, nil
	////case "1":
	////	return 1, nil
	////case "2":
	////	return 2, nil
	////default:
	////	return 2, nil
	////}
}

type Info map[string]string

type OldMsg struct {
	ID       uid.ID `json:"-"`
	Content  string `json:"content"`
	Editable bool   `json:"editable"`
	Include  bool   `json:"include"`
	Visible  bool   `json:"visible"`
	From     int    `json:"from"`
}

type OldMsgs []OldMsg

func (o OldMsgs) ToMsgs(chars OldCharacters) Msgs {
	var userUID, gpt3UID uid.ID
	for i := range chars {
		chars[i].ID = uid.New()
		switch chars[i].Type {
		case 3:
			userUID = chars[i].ID
		case 2:
			gpt3UID = chars[i].ID
		}
	}

	msgs := make([]Msg, len(o))
	for i := range o {
		o[i].ID = uid.New()
	}

	for i, oldMsg := range o {
		msgs[i] = Msg{
			ID:         oldMsg.ID,
			From:       chars[oldMsg.From].ID,
			Content:    oldMsg.Content,
			VisibleTo:  []uid.ID{},
			EditableTo: []uid.ID{},
		}
		if oldMsg.Editable {
			msgs[i].EditableTo = []uid.ID{userUID}
		}
		if oldMsg.Visible {
			msgs[i].VisibleTo = append(msgs[i].VisibleTo, userUID)
		}
		if oldMsg.Include {
			msgs[i].VisibleTo = append(msgs[i].VisibleTo, gpt3UID)
		}
	}
	return msgs
}

type Msg struct {
	ID         uid.ID   `json:"id"`
	From       uid.ID   `json:"from"`
	Content    string   `json:"content"`
	VisibleTo  []uid.ID `json:"visible_to"`  // nil VisibleTo means all allowed
	EditableTo []uid.ID `json:"editable_to"` // nil EditableTo means all allowed
	Visible    bool     `json:"visible"`
	Editable   bool     `json:"editable"`
	Include    bool     `json:"include"`
	Rating     *int     `json:"rating"`
}

func (m Msg) String() string {
	return fmt.Sprintf("message %s from %s and visible to %v and editable to %v: %s", m.ID, m.From, m.VisibleTo, m.EditableTo, m.Content)
}

// RenderForGPT3 renders the message to a string for GPT3
func (m Msg) RenderForGPT3(chars Characters) string {
	char := chars[m.From]
	if char.NameVisible {
		return fmt.Sprintf("%s: %s", chars[m.From].Name, m.Content)
	} else {
		return m.Content
	}
}

type Msgs []Msg

func (m *Msgs) String() string {
	return fmt.Sprintf("%v messages", len(*m))
}

func (m *Msgs) RenderForGPT3(chars Characters) string {
	re := ""
	for _, msg := range *m {
		re += msg.RenderForGPT3(chars) + "\n"
	}
	var gpt3ID uid.ID
	found := false
	// get ID of first GPT-3 char
	for id, char := range chars {
		if char.Type == CharacterTypeGpt3 {
			gpt3ID = id
		}
		found = true
	}
	// GPT-3 char not found?
	if !found {
		panic(errors.New("corrupt conversation: no GPT3 character found"))
	}
	re += fmt.Sprintf("%s:", chars[gpt3ID].Name)
	return re
}

func (m *Msgs) ApproxTokens(chars Characters) uint {
	return uint((strings.Count(m.RenderForGPT3(chars), " ") + 1) * 2) // 1500 tokens roughly 2048 so overcompensate
}

func (m *Msgs) Append(m2 Msg) {
	*m = append(*m, m2)
}

type ConvMeta struct {
	Started time.Time     `json:"started"`
	ID      uid.ID        `json:"id"`
	Active  bool          `json:"active"`
	Engine  string        `json:"engine"` // TODO: deprecate engine in favour of GPT3.Engine
	Timeout time.Duration `json:"timeout"`
	User    uid.ID        `json:"user"`
}

func NewConvMeta(engine string, timeout time.Duration, user uid.ID) ConvMeta {
	return ConvMeta{
		Started: time.Now(),
		ID:      uid.New(),
		Active:  true,
		Engine:  engine,
		Timeout: timeout,
		User:    user,
	}
}

func (c ConvMeta) String() string {
	return fmt.Sprintf("started %v, %s, %v", c.Started, c.ID, c.Active)
}

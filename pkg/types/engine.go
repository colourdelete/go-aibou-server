package types

import (
	"sync"

	"github.com/gin-gonic/gin"
	"gitlab.com/colourdelete/go-aibou-server/pkg/uid"
)

type Engine struct {
	E     *gin.Engine
	Conts map[uid.ID]*ConversationContainer
	Lock  sync.RWMutex
	Addr  []string
}

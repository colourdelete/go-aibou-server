package types

import (
	"embed"
	"errors"
	"fmt"
	"io/fs"
	"io/ioutil"
	"path/filepath"
	"reflect"
	"strings"

	"github.com/sirupsen/logrus"
	"gitlab.com/colourdelete/go-aibou-server/pkg/uid"

	"github.com/traefik/yaegi/interp"
	"github.com/traefik/yaegi/stdlib"
)

//go:embed actions/*
var actionFiles embed.FS

var Actions = map[uid.ID]Action{}

func RunConv(actionID uid.ID, conv *Conversation, trigger string) error {
	action, ok := Actions[actionID]
	if !ok {
		return errors.New("action not found")
	}
	return action.Run(map[string]interface{}{
		"Metadata":     action.Meta,
		"Conversation": conv,
		"Trigger":      trigger,
	})
}

type ActionMeta struct {
	Name string
	ID   uid.ID
}

type Action struct {
	Source string
	Meta   ActionMeta
}

func (a Action) Run(vals map[string]interface{}) error {
	i := interp.New(interp.Options{
		Stdout: logrus.StandardLogger().WriterLevel(logrus.InfoLevel),
		Stderr: logrus.StandardLogger().WriterLevel(logrus.ErrorLevel),
	})
	values := map[string]reflect.Value{}
	for key, val := range vals {
		values[key] = reflect.ValueOf(val)
	}
	i.Use(stdlib.Symbols)
	i.Use(interp.Exports{
		"aibou": values,
	})
	_, err := i.Eval(a.Source)
	return err
}

func init() {
	paths, err := fs.Glob(actionFiles, "actions/*")
	if err != nil {
		panic(err)
	}
	for _, path := range paths {
		file, err := actionFiles.Open(path)
		if err != nil {
			panic(err)
		}

		src, err := ioutil.ReadAll(file)
		if err != nil {
			panic(err)
		}

		base := filepath.Base(path)
		id, err := uid.FromString(strings.ReplaceAll(base[:128], "-", ""))
		if err != nil {
			panic(err)
		}

		Actions[id] = Action{
			Source: string(src),
			Meta: ActionMeta{
				Name: fmt.Sprintf("action %s", id),
				ID:   id,
			},
		}
	}
}

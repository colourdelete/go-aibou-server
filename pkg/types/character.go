package types

import (
	"fmt"

	"gitlab.com/colourdelete/go-aibou-server/pkg/uid"
)

type CharacterType string

const (
	CharacterTypeNarration CharacterType = "NARRATION"
	CharacterTypeInitial   CharacterType = "INITIAL"
	CharacterTypeGpt3      CharacterType = "GPT3"
	CharacterTypeUser      CharacterType = "USER"
)

var AllCharacterType = []CharacterType{
	CharacterTypeNarration,
	CharacterTypeInitial,
	CharacterTypeGpt3,
	CharacterTypeUser,
}

type OldCharacter struct {
	ID          uid.ID `json:"-"`
	Name        string `json:"name"`
	Type        int    `json:"type"`
	NameVisible bool   `json:"add_name_to_gpt"`
}

type Character struct {
	Name        string        `json:"name"`
	Type        CharacterType `json:"type"`
	NameVisible bool          `json:"add_name_to_gpt"`
}

func NewChar(name string, t CharacterType) Character {
	return Character{
		Name: name,
		Type: t,
	}
}

func (c Character) String() string {
	return fmt.Sprintf("(%s) %s", c.Type, c.Name)
}

type OldCharacters []OldCharacter

func (o OldCharacters) ToCharacters() Characters {
	chars := Characters{}
	var t CharacterType
	for _, char := range o {
		switch char.Type {
		case 0:
			t = CharacterTypeNarration
		case 1:
			t = CharacterTypeInitial
		case 2:
			t = CharacterTypeGpt3
		case 3:
			t = CharacterTypeUser
		}
		chars[char.ID] = Character{
			Name:        char.Name,
			Type:        t,
			NameVisible: char.NameVisible,
		}
	}
	return chars
}

type Characters map[uid.ID]Character

func NewChars(n, i, g, u string) Characters {
	return map[uid.ID]Character{
		uid.New(): {n, CharacterTypeNarration, true},
		uid.New(): {i, CharacterTypeInitial, true},
		uid.New(): {g, CharacterTypeGpt3, true},
		uid.New(): {u, CharacterTypeUser, true},
	}
}

func (c Characters) String() string {
	re := ""
	for _, char := range c {
		re += char.String() + "\n"
	}
	return re
}

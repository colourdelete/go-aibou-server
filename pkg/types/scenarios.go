package types

import (
	"embed"
	"encoding/json"
	"fmt"
	"io/fs"
	"path/filepath"
)

//go:embed scenarios/*.json
var scenarios embed.FS

var Scenarios map[string]Scenario
var OldScenarios map[string]OldScenario

type ScenarioInvalid struct {
	Code   string
	Reason string
	Want   interface{}
	Got    interface{}
}

var _ error = ScenarioInvalid{}

func (s ScenarioInvalid) Error() string {
	return fmt.Sprintf("[%s] %s want %#v, got %#v", s.Code, s.Reason, s.Want, s.Got)
}

func verifyScenario(s Scenario) error {
	if s.GPT3.Engine == "" {
		return ScenarioInvalid{
			Code:   "0001",
			Reason: "GPT3.Engine",
			Want:   "(something not blank)",
			Got:    "",
		}
	}
	if s.GPT3.MaxTokens == 0 {
		return ScenarioInvalid{
			Code:   "0002",
			Reason: "GPT3.MaxTokens will fail",
			Want:   "(something not 0)",
			Got:    0,
		}
	}
	if s.Meta.Title == "" {
		return ScenarioInvalid{"0003", "Meta.Title", "(something not blank)", ""}
	}
	if s.Meta.Description == "" {
		return ScenarioInvalid{"0003", "Meta.Description", "(something not blank)", ""}
	}
	if s.Meta.Duration == 0 {
		return ScenarioInvalid{"0003", "Meta.Title", "(something not 0)", 0}
	}
	if s.Meta.Level == 0 {
		return ScenarioInvalid{"0003", "Meta.Level", "(something not 0)", 0}
	}
	if len(s.Chars) < 2 {
		return ScenarioInvalid{"0003", "Chars.len", "(something > 2)", len(s.Chars)}
	}
	return nil
}

func init() {
	matches, err := fs.Glob(scenarios, "scenarios/*.json")
	if err != nil {
		panic(fmt.Sprintf("glob: %s", err))
	}
	var data []byte
	Scenarios = map[string]Scenario{}
	OldScenarios = map[string]OldScenario{}
	for i, match := range matches {
		data, err = scenarios.ReadFile(match)
		if err != nil {
			panic(err)
		}
		path_ := filepath.Base(match)
		path := path_[:len(path_)-len(filepath.Ext(path_))]
		scenario := OldScenario{}
		err = json.Unmarshal(data, &scenario)
		if err != nil {
			panic(fmt.Sprintf("%v %v json unmarshal: %s", i, match, err))
		}
		OldScenarios[path] = scenario
		newScenario := scenario.ToScenario()
		Scenarios[path] = newScenario

		if err := verifyScenario(newScenario); err != nil {
			panic(fmt.Sprintf("%v %v verify scenario: %s", i, match, err))
		}
	}
}

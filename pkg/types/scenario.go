package types

import (
	"fmt"
	"time"

	"github.com/PullRequestInc/go-gpt3"
)

// For struct tags, use `toml` for data defined in data files (E.g. `example_action.toml`).
// Use `json` for data to be serialized to the client

type GPT3 struct {
	SummarizeToken   int     `json:"summarize_token"`
	ResponseLength   int     `json:"response_length"`
	Temperature      float32 `json:"temperature"`
	TopP             float32 `json:"top_p"`
	FrequencyPenalty float32 `json:"frequency_penalty"`
	PresencePenalty  float32 `json:"presence_penalty"`
	MaxTokens        int     `json:"max_tokens"`
	Engine           string  `json:"engine"`
	N                int     `json:"n"`
	LogProbs         float32 `json:"log_probs"`
	APIKey           string  `json:"-"` // make sure to not send secrets lol
}

var _ fmt.Stringer = GPT3{}

func (g GPT3) String() string {
	return fmt.Sprintf(
		"summarize token: %v, response len: %v, temp: %v, top p: %v, freq penalty: %v, prescence penalty: %v",
		g.SummarizeToken,
		g.ResponseLength,
		g.Temperature,
		g.TopP,
		g.FrequencyPenalty,
		g.PresencePenalty,
	)
}

func (g GPT3) NewClient() gpt3.Client {
	return gpt3.NewClient(g.APIKey)
}

type ScenarioMeta struct {
	Title       string        `son:"title"`
	Description string        `json:"description"`
	Duration    time.Duration `json:"duration"`
	Level       int           `json:"level"`
}

var _ fmt.Stringer = ScenarioMeta{}

func (s ScenarioMeta) String() string {
	return fmt.Sprintf("%s, level %v, duration: %v", s.Title, s.Level, s.Duration)
}

type OldScenario struct {
	Meta  ScenarioMeta  `json:"meta"`
	Info  Info          `json:"info"`
	Msgs  OldMsgs       `json:"initial_messages"`
	Chars OldCharacters `json:"message_types"` // only support one char per type
	GPT3  GPT3          `json:"gpt3"`          // conform to scenarios/*.json
}

func (o OldScenario) ToScenario() Scenario {
	msgs := o.Msgs.ToMsgs(o.Chars)
	return Scenario{
		Meta:  o.Meta,
		Info:  o.Info,
		Msgs:  &msgs,
		Chars: o.Chars.ToCharacters(),
		GPT3:  o.GPT3,
	}
}

type Scenario struct {
	Meta  ScenarioMeta `json:"meta"`
	Info  Info         `json:"info"`
	Msgs  *Msgs        `json:"initial_messages"`
	Chars Characters   `json:"message_types"` // only support one char per type
	GPT3  GPT3         `json:"gpt_3"`
	Actions []OldAction `json:"actions"`
}

type OldAction struct {
	Desc string `json:"desc"`
	Enabled bool `json:"enabled"`
}

var _ fmt.Stringer = Scenario{}

func (s Scenario) String() string {
	return fmt.Sprintf("scenario %s, %s, %s, %s, %s", s.Meta, s.Info, s.Msgs, s.Chars, s.GPT3)
}

type ScenarioSummary struct {
	Meta *ScenarioMeta `json:"meta"`
}

var _ fmt.Stringer = ScenarioSummary{}

func (s ScenarioSummary) String() string {
	return fmt.Sprintf("summary %s", s.Meta)
}

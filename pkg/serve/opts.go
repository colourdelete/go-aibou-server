package serve

type Opts struct {
	ConfigPath string `short:"c" long:"config" default:""`
	Port       int    `short:"p" long:"port" default:"-1"`
	Release    bool   `short:"r" long:"release"`
	Log        string `short:"l" long:"log" default:"log"`
}

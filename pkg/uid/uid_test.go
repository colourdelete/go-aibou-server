package uid

import (
	"testing"
)

func TestNew(t *testing.T) {
	err := Validate(New())
	if err != nil {
		t.Fatal(err)
	}
}

func TestValidate(t *testing.T) {
	t.Run("invalid", func(t *testing.T) {
		if Validate("abcdef") == nil {
			t.Fatal("must not be nil")
		}
	})
	t.Run("valid", func(t *testing.T) {
		if Validate("316098eb23bfa8573502eabec2931e5f793defb3755a0e6e610a623aa35ebebb8625a042a17b3b1747800e7ec5fc6ca81dd17923830158c57dac4e825d5548e1") != nil {
			t.Fatal("must not be nil")
		}
	})
}

package server

import (
	"net/http"

	"github.com/gin-contrib/cors"

	"gitlab.com/colourdelete/go-aibou-server/pkg/uid"

	"gitlab.com/colourdelete/go-aibou-server/pkg/aliases"

	"gitlab.com/colourdelete/go-aibou-server/pkg/graphql"

	auth2 "gitlab.com/colourdelete/go-aibou-server/pkg/auth"

	"gitlab.com/colourdelete/go-aibou-server/pkg/types"

	"github.com/sirupsen/logrus"

	"github.com/gin-gonic/gin"
	"go.uber.org/ratelimit"
)

func init() {
	logrus.SetOutput(gin.DefaultWriter)
}

func leakBucket(limit ratelimit.Limiter) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		limit.Take()
		ctx.Next()
	}
}

// Setup makes a new Gin router and returns an Engine.
func Setup(rps int, corsConfig *cors.Config, addr ...string) (e *types.Engine) {
	router := gin.New()

	// Make Engine
	e = &types.Engine{
		E:     router,
		Conts: map[uid.ID]*types.ConversationContainer{},
		Addr:  addr,
	}

	// Setup 405
	router.HandleMethodNotAllowed = true
	router.NoMethod(func(ctx *gin.Context) {
		ctx.JSON(http.StatusMethodNotAllowed, types.Resp{
			Msg:  "method not allowed",
			Data: nil,
		})
	})

	// Setup 404
	router.NoRoute(func(ctx *gin.Context) {
		ctx.JSON(http.StatusNotFound, types.Resp{
			Msg:  "not found",
			Data: ctx.Request.URL.String(),
		})
	})

	// Middleware
	// ==========

	// Setup Logger, Recovery, GraphQL Middleware
	router.Use(
		gin.Logger(),
		gin.Recovery(),
		graphql.GinContextToContextMiddleware(), // used in GraphQL
	)

	// Setup CORS Middleware if present
	if corsConfig != nil {
		router.Use(cors.New(*corsConfig))
	}

	// Setup Ratelimit Middleware
	if rps > 0 {
		router.Use(leakBucket(ratelimit.New(rps)))
	}

	// V2 Group
	v2 := router.Group("/v2")

	// Setup JWT Middleware
	authMiddleware := auth2.SetupJWT(router, false)
	requireAuth := v2.Group("/", authMiddleware)

	// Setup GraphQL "package"
	graphql.Engine = e

	// Setup GraphQL Routes
	requireAuth.POST(graphql.QueryEndpoint, graphql.QueryHandler())
	if gin.Mode() == gin.DebugMode {
		requireAuth.GET(graphql.PlaygroundEndpoint, graphql.PlaygroundHandler())
	}

	// V1 Aliases
	v1 := router.Group("/v1", authMiddleware)
	v1.GET("/meta", aliases.Meta)
	v1.GET("/scenarios", aliases.Scenarios)
	v1.GET("/scenario/:scenario_id", aliases.Scenario)
	v1.POST("/convs", aliases.NewConversation)
	v1.GET("/convs/:conversation_id", aliases.Conversation)
	v1.DELETE("/convs/:conversation_id", aliases.DelConversation)
	v1.POST("/conversation/:conversation_id/msg", aliases.NewMessage)
	v1.PATCH("/conv/:conversation_id/msgs/:message_id", aliases.EditMessage)       // TODO: failed in Postman
	v1.POST("/conversation/:conversation_id/actions/:action_id", aliases.DoAction) // TODO: failed in Postman

	router.GET("/", func(ctx *gin.Context) { // shields.io
		ctx.JSON(http.StatusOK, gin.H{
			"status": "online",
		})
	})

	return e
}

// RunEngine runs the Engine.
func RunEngine(e *types.Engine) error {
	return e.E.Run(e.Addr...)
}

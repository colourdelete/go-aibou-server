package data

import (
	_ "embed"
	"fmt"
	"time"
)

const Name = "Go AIbou Server"

var Authors = []string{
	"Ken Shibata <colourdelete@gmail.com>",
}

var Text = fmt.Sprintf(
	"%s version %s (hash %s, built %s) by %s",
	Name,
	Version,
	Hash,
	Timestamp,
	Authors,
)

//go:embed hash.txt
var Hash string

//go:embed version.txt
var Version string

//go:embed time.txt
var Timestamp string

//go:embed secret_key.txt
var SecretKey []byte

//go:embed realm.txt
var Realm string

const Timeout = time.Hour
const MaxRefresh = time.Hour

func init() {
	if len(SecretKey) < 64 {
		panic(fmt.Sprintf("secret key length is less than 64: %s", SecretKey))
	}
}

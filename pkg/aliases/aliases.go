package aliases

import (
	"fmt"
	"net/http"

	"gitlab.com/colourdelete/go-aibou-server/pkg/uid"

	"github.com/gin-gonic/gin"
	"gitlab.com/colourdelete/go-aibou-server/pkg/graphql"
	"gitlab.com/colourdelete/go-aibou-server/pkg/types"
)

var root = graphql.Root{}

func shouldBindError(err error) (int, types.Resp) {
	return http.StatusInternalServerError, types.Resp{
		Msg:  err.Error(),
		Data: err,
	}
}

func Meta(ctx *gin.Context) {
	meta, err := root.Query().Meta(ctx.Request.Context())
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, types.Resp{
			Msg:  err.Error(),
			Data: err,
		})
		return
	}
	ctx.JSON(http.StatusOK, types.Resp{
		Msg:  "ok",
		Data: meta,
	})
}

func Scenarios(ctx *gin.Context) { // TODO: return format compatible with v1
	scenarios, err := root.Query().Scenario(ctx.Request.Context(), nil)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, types.Resp{
			Msg:  err.Error(),
			Data: err,
		})
		return
	}
	ctx.JSON(http.StatusOK, gin.H{
		"scenarios": scenarios,
	})
}

type ScenarioReq struct {
	ScenarioID uid.ID `uri:"scenario_id" binding:"required"`
}

func Scenario(ctx *gin.Context) { // TODO: return format compatible with v1
	req := ScenarioReq{}
	if err := ctx.ShouldBindUri(&req); err != nil {
		ctx.JSON(shouldBindError(err))
		return
	}
	scenarios, err := root.Query().Scenario(ctx.Request.Context(), []uid.ID{
		req.ScenarioID,
	})
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, types.Resp{
			Msg:  err.Error(),
			Data: err,
		})
		return
	}
	ctx.JSON(http.StatusOK, gin.H{
		"scenario": scenarios[0],
	})
}

type NewConversationReq struct {
	ScenarioID *int `json:"scenario_number" binding:"required"`
}

func NewConversation(ctx *gin.Context) {
	req := NewConversationReq{}
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(shouldBindError(err))
		return
	}
	scenariosList := make([]uid.ID, 0)
	for key := range types.Scenarios {
		if key == fmt.Sprintf("%d", *req.ScenarioID) {
			scenariosList = append(scenariosList, uid.ID(key))
		}
	}
	conversationID, err := root.Mutation().NewConversation(ctx.Request.Context(), scenariosList[*req.ScenarioID])
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, types.Resp{
			Msg:  err.Error(),
			Data: err,
		})
		return
	}

	ctx.JSON(http.StatusOK, gin.H{
		"conv_id": conversationID,
		"type":    "ok",
	})
}

type ConversationReq struct {
	ConversationID uid.ID `uri:"conversation_id" binding:"required"`
}

func Conversation(ctx *gin.Context) {
	req := ConversationReq{}
	if err := ctx.ShouldBindUri(&req); err != nil {
		ctx.JSON(shouldBindError(err))
		return
	}
	conversations, err := root.Query().Conversations(ctx.Request.Context(), []uid.ID{
		req.ConversationID,
	})
	if len(conversations) != 1 {
		ctx.JSON(http.StatusInternalServerError, types.Resp{
			Msg:  fmt.Sprintf("conversations len should be 1, but was %v", len(conversations)),
			Data: conversations, // should not be security issue since client can already access these
		})
		return
	}

	if err != nil {
		ctx.JSON(http.StatusInternalServerError, types.Resp{
			Msg:  err.Error(),
			Data: err,
		})
		return
	}
	ctx.JSON(http.StatusOK, gin.H{
		"conversation": conversations[0],
		"type":         "ok",
	})
}

type DelConversationReq struct {
	ConversationID uid.ID `uri:"conversation_id" binding:"required"`
}

func DelConversation(ctx *gin.Context) {
	ctx.JSON(http.StatusNotImplemented, gin.H{})
	//req := ConversationReq{}
	//if err := ctx.ShouldBindUri(&req); err != nil {
	//	ctx.JSON(shouldBindError(err))
	//	return
	//}
	//conversations, err := root.Mutation().DelConversation(ctx.Request.Context(), req.ConversationID)
	//if len(conversations) != 1 {
	//	ctx.JSON(http.StatusInternalServerError, types.Resp{
	//		Msg:  fmt.Sprintf("conversations len should be 1, but was %v", len(conversations)),
	//		Data: conversations, // should not be security issue since client can already access these
	//	})
	//}
	//
	//if err != nil {
	//	ctx.JSON(http.StatusInternalServerError, types.Resp{
	//		Msg:  err.Error(),
	//		Data: err,
	//	})
	//	return
	//}
	//ctx.JSON(http.StatusOK, gin.H{
	//	"conversation": conversations[0],
	//	"type":         "ok",
	//})
}

type NewMessageReq struct {
	ConversationID uid.ID `uri:"conversation_id" binding:"required"`
}

type NewMessageBody struct {
	Message LegacyMessage `json:"msg"`
}

type LegacyMessage struct {
	From     int    `json:"from"`
	Content  string `json:"content"`
	Visible  bool   `json:"visible"`
	Editable bool   `json:"editable"`
}

func NewMessage(ctx *gin.Context) {
	req := NewMessageReq{}
	if err := ctx.ShouldBindUri(&req); err != nil {
		ctx.JSON(shouldBindError(err))
		return
	}
	body := NewMessageBody{}
	if err := ctx.ShouldBindJSON(&body); err != nil {
		ctx.JSON(shouldBindError(err))
		return
	}

	conversations, err := root.Query().Conversations(ctx.Request.Context(), []uid.ID{
		req.ConversationID,
	})
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, types.Resp{
			Msg:  err.Error(),
			Data: err,
		})
		return
	}
	if len(conversations) == 0 {
		ctx.JSON(http.StatusNotFound, types.Resp{
			Msg: "no conversation returned from internal GraphQL request for processing",
			Data: struct {
				Conversations []*graphql.Conversation `json:"conversations"`
				Error         error                   `json:"error"`
			}{
				Conversations: conversations,
				Error:         err,
			},
		})
		return
	}

	conversation := conversations[0]

	var fromID uid.ID
	var userID uid.ID

	for _, character := range conversation.Characters {
		ct := map[int]types.CharacterType{
			0: types.CharacterTypeNarration,
			1: types.CharacterTypeInitial,
			2: types.CharacterTypeUser,
			3: types.CharacterTypeGpt3,
		}
		if character.Type == ct[body.Message.From] {
			fromID = character.ID
		}
		if character.Type == types.CharacterTypeUser {
			userID = character.ID
		}
	}

	// use GraphQL method
	ok, err := root.Mutation().NewMessage(ctx.Request.Context(), req.ConversationID, graphql.MessageInput{
		From:       fromID,
		Content:    body.Message.Content,
		VisibleTo:  []uid.ID{userID},
		EditableTo: []uid.ID{userID},
	})
	// NOTE: find out what this does
	//for _, msg := range conversation.Messages {
	//	chars := types.Characters{}
	//	for _, char := range conversation.Characters {
	//		chars[char.ID] = types.Character{
	//			Name: char.Name,
	//			Type: char.Type,
	//		}
	//	}
	//	msg.Include
	//}

	if !ok || err != nil {
		if err != nil && err.Error() == "unsafe or sensitive" {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"error": "inapppropriate message",
			})
		}
		if err != nil {
			ctx.JSON(http.StatusInternalServerError, gin.H{
				"error": err,
				"msg":   err.Error(),
				"ok":    ok,
				"type":  "error",
			})
		} else {
			ctx.JSON(http.StatusInternalServerError, gin.H{
				"ok":   false,
				"type": "error",
			})
		}
		return
	}

	conversations, err = root.Query().Conversations(ctx.Request.Context(), []uid.ID{
		req.ConversationID,
	})
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, types.Resp{
			Msg:  err.Error(),
			Data: err,
		})
		return
	}
	if len(conversations) == 0 {
		ctx.JSON(http.StatusNotFound, types.Resp{
			Msg: "no conversation returned from internal GraphQL request for response",
			Data: struct {
				Conversations []*graphql.Conversation `json:"conversations"`
				Error         error                   `json:"error"`
			}{
				Conversations: conversations,
				Error:         err,
			},
		})
		return
	}

	c := conversations[0]

	ctx.JSON(http.StatusOK, gin.H{
		"conversation": c,
		"type":         "ok",
	})
}

type EditMessageReq struct {
	ConversationID uid.ID `uri:"conversation_id" binding:"required"`
	MessageID      int    `uri:"message_id" binding:"required"`
}

func EditMessage(ctx *gin.Context) {
	req := EditMessageReq{}
	if err := ctx.ShouldBindUri(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, types.Resp{
			Msg:  err.Error(),
			Data: err,
		})
		return
	}

	body := graphql.MessageEditInput{}
	if err := ctx.ShouldBindUri(&body); err != nil {
		ctx.JSON(http.StatusBadRequest, types.Resp{
			Msg:  err.Error(),
			Data: err,
		})
		return
	}

	conversations, err := root.Query().Conversations(ctx.Request.Context(), []uid.ID{req.ConversationID})

	if err != nil {
		ctx.JSON(http.StatusBadRequest, types.Resp{
			Msg:  err.Error(),
			Data: err,
		})
		return
	}
	if len(conversations) != 1 {
		ctx.JSON(http.StatusInternalServerError, types.Resp{
			Msg:  "internal GQL query returned with non 1 length",
			Data: conversations,
		})
		return
	}
	conversation := conversations[0]

	messageID := conversation.Messages[req.MessageID].ID

	ok, err := root.Mutation().EditMessage(ctx.Request.Context(), req.ConversationID, messageID, body)

	if !ok || err != nil {
		ctx.JSON(http.StatusInternalServerError, types.Resp{
			Msg:  fmt.Sprintf("%v or %s", ok, err),
			Data: err,
		})
	}
	ctx.JSON(http.StatusOK, gin.H{
		"type": "ok",
	})
}

type DoActionReq struct {
	ConversationID uid.ID `uri:"conversation_id" binding:"required"`
	ActionID       int    `uri:"action_id" binding:"required"`
}

func DoAction(ctx *gin.Context) {
	req := DoActionReq{}
	if err := ctx.ShouldBindUri(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, types.Resp{
			Msg:  err.Error(),
			Data: err,
		})
		return
	}

	conversations, err := root.Query().Conversations(ctx.Request.Context(), []uid.ID{req.ConversationID})
	if err != nil {
		ctx.JSON(http.StatusBadRequest, types.Resp{
			Msg:  err.Error(),
			Data: err,
		})
		return
	}
	if len(conversations) != 1 {
		ctx.JSON(http.StatusInternalServerError, types.Resp{
			Msg:  "internal GQL query returned with non 1 length",
			Data: conversations,
		})
		return
	}
	conversation := conversations[0]

	switch req.ActionID {
	case 0: // end conv
		// make conv inactive
		conversation.Meta.Active = false // TODO: cleanup
	case 1: // reask gpt3
		// delete latest msg from gpt3
		// refresh
		conversation.Messages = conversation.Messages[:len(conversation.Messages)-1]
	}

	//ok, err := root.Mutation().TriggerAction(ctx.Request.Context(), req.ConversationID, req.ActionID)
	//if !ok || err != nil {
	//	ctx.JSON(http.StatusInternalServerError, types.Resp{
	//		Msg:  fmt.Sprintf("%v or %s", ok, err),
	//		Data: err,
	//	})
	//	return
	//} else {
	//	conversations, err := root.Query().Conversations(ctx.Request.Context(), []uid.ID{req.ConversationID})
	//
	//	if err != nil {
	//		ctx.JSON(http.StatusBadRequest, types.Resp{
	//			Msg:  err.Error(),
	//			Data: err,
	//		})
	//		return
	//	}
	//	if len(conversations) != 1 {
	//		ctx.JSON(http.StatusInternalServerError, types.Resp{
	//			Msg:  "internal GQL query returned with non 1 length",
	//			Data: conversations,
	//		})
	//		return
	//	}
	//
	//	ctx.JSON(http.StatusBadRequest, gin.H{
	//		"conversation":        conversations[0],
	//		"reload_all_messages": true, // actually check
	//	})
	//	return
	//}
}

package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"

	"github.com/pelletier/go-toml"
)

var pattern string
var inPlace bool
var checkOnly bool

func main() {
	flag.StringVar(&pattern, "pattern", "", "pattern to format")
	flag.BoolVar(&inPlace, "in-place", false, "format in place?")
	flag.BoolVar(&checkOnly, "check-only", false, "only check? (if true doesn't output nor change in place)")
	flag.Parse()

	matches, err := filepath.Glob(pattern)
	if err != nil {
		fmt.Printf("✗ glob: %s\n", err)
		os.Exit(1)
	}
	fmt.Printf("i %v matches\n", len(matches))

	for _, match := range matches {
		src, err := ioutil.ReadFile(match)
		if err != nil {
			fmt.Printf("✗ %s read: %s\n", match, err)
			os.Exit(1)
		}

		var data interface{}
		err = toml.Unmarshal(src, &data)
		if err != nil {
			fmt.Printf("✗ %s toml: %s\n", match, err)
			os.Exit(2)
		}

		dest, err := toml.Marshal(data)
		if err != nil {
			fmt.Printf("✗ %s toml: %s\n", match, err)
			os.Exit(2)
		}

		if !checkOnly {
			if inPlace {
				err = ioutil.WriteFile(match, dest, 0644)
			} else {
				_, err = os.Stdout.Write(dest)
			}
			if err != nil {
				fmt.Printf("✗ %s write: %s\n", match, err)
				os.Exit(1)
			}
		}
		fmt.Printf("✓ %s\n", match)
	}

}
